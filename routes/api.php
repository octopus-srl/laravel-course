<?php

use Illuminate\Support\Facades\Route;
use Src255\Http\Controllers\v1\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'posts'], function () {

        Route::get('', [PostController::class, 'index'])->name('v1.posts.index');
        Route::post('', [PostController::class, 'store'])->name('v1.posts.store');
        Route::get('{id}', [PostController::class, 'show'])->name('v1.posts.show');

    });

});



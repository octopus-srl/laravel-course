<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Src255\Models\Post;
use Src255\Models\User;


class PostFactory extends Factory
{
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->text(100),
            'text' =>  $this->faker->text(300),
        ];
    }
}

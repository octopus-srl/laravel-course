<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Src255\Models\PostCategory;

class PostCategoryFactory extends Factory
{

    protected $model = PostCategory::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'label' => $this->faker->text(100),
        ];
    }
}

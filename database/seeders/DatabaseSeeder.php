<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Src255\Models\Post;
use Src255\Models\PostCategory;
use Src255\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \User::factory(10)->create();

        // \User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::factory()
            ->has(
                Post::factory()
                    ->has(PostCategory::factory()->count(2), 'categories')
                    ->count(20)
            )
            ->count(10)
            ->create();
    }
}

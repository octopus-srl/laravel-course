<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('author_id')->index()->nullable();
            $table->string('title');
            $table->text('text')->nullable();
            $table->timestamps();

            $table->foreign('author_id')->references('id')
                ->on('users')->onDelete('set null');
        });

        Schema::create('post_categories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('label');
            $table->timestamps();
        });

        Schema::create('post_category_post', function (Blueprint $table) {
            $table->uuid('post_id')->index();
            $table->uuid('post_category_id')->index();
            $table->boolean('default')->default(true);

            $table->foreign('post_id')->references('id')
                ->on('posts')->onDelete('cascade');

            $table->foreign('post_category_id')->references('id')
                ->on('post_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_categories');
        Schema::dropIfExists('posts');
    }
};

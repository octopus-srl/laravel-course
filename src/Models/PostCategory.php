<?php

namespace Src255\Models;

use Database\Factories\PostCategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Src255\Models\Traits\HasUuid;

/**
 * Src255\Models\PostCategory
 *
 * @property string $id
 * @property string $label
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\PostCategoryFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|PostCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PostCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PostCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|PostCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostCategory whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Src255\Models\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Src255\Models\Post[] $postsWithPivot
 * @property-read int|null $posts_with_pivot_count
 */
class PostCategory extends Model
{
    use HasFactory;
    use HasUuid;

    protected $table = 'post_categories';

    protected $fillable = [
        'label',
    ];

    protected static function newFactory(): PostCategoryFactory
    {
        return PostCategoryFactory::new();
    }

    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'post_category_post');
    }

    public function postsWithPivot(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'post_category_post')
            ->withPivot(['default']);
    }
}

<?php

namespace Src255\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Src255\Http\Requests\v1\PostStoreRequest;
use Src255\Http\Resources\v1\PostResource;
use Src255\Interfaces\PostServiceInterface;
use Src255\Models\Post;

class PostController extends Controller
{
    public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {

        $posts = Post::with('author')->paginate(5);

        return PostResource::collection($posts);
    }

    public function show(string $id): PostResource
    {
        $post = Post::findOrFail($id);

        return new PostResource($post);
    }

    public function store(PostStoreRequest $request, PostServiceInterface $postService): PostResource
    {
        $fields = $request->validated();

        $post = $postService->storePost(
            Arr::get($fields, 'title', ''),
            Arr::get($fields, 'text', ''),
        );

        return new PostResource($post);

    }
}

<?php

namespace Src255\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;
use Src255\Models\Post;

/**
 * @mixin Post
 */
class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array|\JsonSerializable|\Illuminate\Contracts\Support\Arrayable
    {
        return [
            'title' => $this->title,
            'text' => $this->text,
            'author' => new UserResource($this->whenLoaded('author'))
        ];
    }
}

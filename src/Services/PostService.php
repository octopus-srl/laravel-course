<?php

namespace Src255\Services;

use Src255\Interfaces\PostServiceInterface;
use Src255\Models\Post;

class PostService implements PostServiceInterface
{

    public function storePost(string $title, string $text): Post
    {
       $post = new Post();
       $post->title = $title;
       $post->text = $text;

       $post->save();

       return $post;

    }
}

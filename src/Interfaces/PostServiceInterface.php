<?php

namespace Src255\Interfaces;

use Src255\Models\Post;

interface PostServiceInterface
{
    public function storePost(string $title, string $text): Post;
}

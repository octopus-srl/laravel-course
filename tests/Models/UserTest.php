<?php

namespace Tests\Models;

use Illuminate\Database\Eloquent\Collection;
use Src255\Models\Post;
use Src255\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{

    public function testShouldBeInit()
    {
        $user = User::factory()->state([
            'name' => 'ciccio'
        ])->create();

        $this->assertInstanceOf(User::class, $user);

        $this->assertDatabaseHas('users', ['name' => 'ciccio']);
    }

    public function testUserShouldHaveManyPosts()
    {
        $user = User::factory()
            ->has(Post::factory()->count(3))
            ->create();

        $this->assertCount(3, $user->posts);

        $this->assertInstanceOf(Collection::class, $user->posts);

        $this->assertInstanceOf(Post::class, $user->posts->first());
    }

}

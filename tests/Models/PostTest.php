<?php

namespace Tests\Models;

use Illuminate\Database\Eloquent\Collection;
use Src255\Models\Post;
use Src255\Models\PostCategory;
use Src255\Models\User;
use Tests\TestCase;

class PostTest extends TestCase
{

    public function testPostShouldBelongToUser()
    {
        /** @var Post $post */
        $post = Post::factory()
            ->for(User::factory(), 'author')
            ->create();

        $this->assertInstanceOf(User::class, $post->author);
    }

    public function testPostShouldBelongsToManyCategories()
    {
        /** @var Post $post */
        $post = Post::factory()
            ->has(PostCategory::factory()->count(2), 'categories')
            ->create();

        $this->assertInstanceOf(Collection::class, $post->categories);

        $this->assertInstanceOf(PostCategory::class, $post->categories->first());
    }

}

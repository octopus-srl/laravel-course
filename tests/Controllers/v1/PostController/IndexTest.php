<?php

namespace Tests\Controllers\v1\PostController;


use Src255\Models\Post;
use Tests\TestCase;

class IndexTest extends TestCase
{

    public function testShouldHavePosts()
    {
        Post::factory()
            ->count(10)
            ->create();

        $response = $this->json('GET', 'api/v1/posts');

        $response->assertSuccessful();
    }


}

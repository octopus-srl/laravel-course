<?php

namespace Tests\Controllers\v1\PostController;

use Illuminate\Support\Str;
use Src255\Models\Post;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ShowTest extends TestCase
{

    public function testShouldHavePost()
    {
        $post = Post::factory()
            ->create();

        $response = $this->json('GET', sprintf('api/v1/posts/%s', $post->id ));

        $response->assertSuccessful();
    }


    public function testShouldHaveNotFound()
    {

        $response = $this->json('GET', sprintf('api/v1/posts/%s', Str::uuid()->toString() ));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }


}

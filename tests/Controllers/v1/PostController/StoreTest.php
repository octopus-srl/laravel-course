<?php

namespace Tests\Controllers\v1\PostController;

use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class StoreTest extends TestCase
{

    public function testShouldCreatePost()
    {

        $response = $this->json('POST', 'api/v1/posts', [
            'title' => 'title',
            'text' => 'text'
        ]);

        $response->assertSuccessful();
    }

    public function testShouldNotCreatePost()
    {

        $response = $this->json('POST', 'api/v1/posts', [
            'title' => 'title'
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }


}

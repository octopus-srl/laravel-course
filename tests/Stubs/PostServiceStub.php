<?php

namespace Tests\Stubs;

use Src255\Interfaces\PostServiceInterface;
use Src255\Models\Post;

class PostServiceStub implements PostServiceInterface
{

    public function storePost(string $title, string $text): Post
    {
        return Post::factory()->create();
    }
}

<?php

namespace Tests\Services\PostService;

use Src255\Models\Post;
use Src255\Services\PostService;
use Tests\TestCase;

class StorePostTest extends TestCase
{

    public function testShouldSavePost()
    {
        $service = new PostService();

        $post = $service->storePost('title', 'text');

        $this->assertInstanceOf(Post::class, $post);

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'title' => 'title',
            'text' => 'text'
        ]);

        $this->assertModelExists($post);
    }

}

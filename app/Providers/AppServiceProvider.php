<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Src255\Interfaces\PostServiceInterface;
use Src255\Services\PostService;
use Tests\Stubs\PostServiceStub;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() === 'testing') {
            $this->app->bind(PostServiceInterface::class, PostServiceStub::class);
        } else {
            $this->app->bind(PostServiceInterface::class, PostService::class);
        }

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

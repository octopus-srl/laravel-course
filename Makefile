# Static ———————————————————————————————————————————————————————————————————————————————————————————————————————————————
LC_LANG = it_IT

# Setup ————————————————————————————————————————————————————————————————————————————————————————————————————————————————
php_container := app
args = $(filter-out $@,$(MAKECMDGOALS))
docker_exec := docker-compose exec

enter:
	$(docker_exec) $(php_container) bash
.SILENT: enter

clear-cache:
	$(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan clear-compiled;\
    $(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan cache:clear;\
    $(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan config:clear;\
    $(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan event:clear;\
    $(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan optimize:clear;\
    $(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan route:clear;\
    $(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan view:clear
.SILENT: clear-cache

reset-db:
	$(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan db:wipe;\
    $(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan migrate
.SILENT: reset-db

seed-db:
	$(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan module:seed Core
.SILENT: seed-db

test: export DB_HOST = database_test
test: export APP_ENV = testing
test:
	#make clear-cache
	#make reset-db
	$(docker_exec) -e APP_ENV -e DB_HOST $(php_container) php artisan test --stop-on-failure $(call args);
.SILENT: test

infection: export DB_HOST = database_test
infection: export APP_ENV = test
infection:
	make clear-cache
	make reset-db
	$(docker_exec) -e APP_ENV -e DB_HOST $(php_container)  vendor/bin/infection --only-covered --threads=4
.SILENT: infection

security-check:
	$(docker_exec) $(php_container) php vendor/bin/security-checker security:check composer.lock
.SILENT: security-check

phpstan:
	$(docker_exec) $(php_container) vendor/bin/phpstan analyse -c phpstan.neon -vvv --memory-limit=2048M
.PHONY: phpstan

pint:
	$(docker_exec) $(php_container) vendor/bin/pint --config pint.json
.PHONY: pint

ide-helper:
	$(docker_exec) $(php_container) php artisan clear-compiled ;\
    $(docker_exec) $(php_container) php artisan ide-helper:generate ;\
    $(docker_exec) $(php_container) php artisan ide-helper:models -W ;\
    $(docker_exec) $(php_container) php artisan ide-helper:meta
.SILENT: ide-helper
